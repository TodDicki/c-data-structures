#include <iostream>
#include <time.h>
#include "BST.h"


int main()
{
	
	srand(time(nullptr));//random seed

	BST<int>* bst = new BST<int>();//create binary tree

	clock_t t = clock();//clock for measuring time.

	std::cout << "Inserting values." << std::endl;

	int toFind;//integer to find later

	//insert random values into the binary tree
	for (int i = 0; i < 1000000; i++)
	{
		toFind = rand() % 500000 + 1;
		bst->Insert(toFind);
	}

	//Display the found value, as well as the maximum and minimum
	std::cout << "Found: " << (bst->Search(toFind))->value << std::endl;
	std::cout << "Maximum: " << bst->Maximum()->value << std::endl;
	std::cout << "Minimum: " << bst->Minimum()->value << std::endl;

	//Remove the maximum and minimum values
	bst->Remove(bst->Maximum()->value);
	bst->Remove(bst->Minimum()->value);
	std::cout << "Deleting maximum and minimum value." << std::endl;

	//Display the new maximum and minimum value
	std::cout << "Maximum: " << bst->Maximum()->value << std::endl;
	std::cout << "Minimum: " << bst->Minimum()->value << std::endl;



	t = clock() - t;
	std::cout << "Time: " << ((float)t / CLOCKS_PER_SEC) << std::endl;

	//wait for user input
	std::cin.get();

	//exit function
	return 0;
}