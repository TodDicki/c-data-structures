#ifndef _BST_H_
#define _BST_H_

template<typename T>
struct node
{
	T value;//value of this node
	node* left;//node to the left of this node
	node* right;//node to the right of this node
	node* parent;//parent node of this node
	int height;//height of the node
};

template<typename T>
class BST
{
public:

	/// <summary> Default Constructor </summary>
	BST()
	{
		root = nullptr;//Default root to null.
	}

	/// <summary> Default Deconstructor </summary>
	~BST()
	{
		DestroyTree();//Destroy the tree.
	}

	/// <summary> Insert a key into the BST. </summary>
	/// <param name="key"> Value to be inserted into the tree. </param>
	void Insert(T key)
	{
		if (root != nullptr)//if the root exists
		{
			Insert(key, root);//insert the value
		}
		else//otherwise
		{
			//Create the root and assign the value.
			root = new node<T>();
			root->value = key;
			root->left = nullptr;
			root->right = nullptr;
			root->parent = nullptr;
			root->height = 1;
		}
	}

	/// <summary> Finds a node with the value. </summary>
	/// <param name="key"> Value to search for </param>
	/// <returns> Returns node with key value. </returns>
	node<T>* Search(T key)
	{
		return Search(key, root);
	}

	/// <summary> Destroys the BST. </summary>
	void DestroyTree()
	{
		DestroyTree(root);
	}	

	/// <summary> Finds the node with the maximum value. </summary>
	/// <returns> Returns node with maximum value. </returns>
	node<T>* Maximum()
	{
		return Maximum(root);
	}

	/// <summary> Finds the node with the minimum value. </summary>
	/// <returns> Returns node with minimum value. </returns>
	node<T>* Minimum()
	{
		return Minimum(root);
	}

	/// <summary> Removes a node with the value. </summary>
	/// <param name="key"> Value to remove </param>
	void Remove(T key)
	{
		if (root == nullptr)//if the root is empty
			return;//do nothing
		else//otherwise
		{
			if (root->value == key)//if the roots value is the key
			{
				node<T>* auxRoot = new node<T>();//create a temporary root
				auxRoot->left = root;//set the new roots left to the old root - Done so the root will have a parent node for the recursive removal
				node<T>* removedNode = Remove(key, root);//find the node to remove
				root = auxRoot->left;
				if (removedNode != nullptr)//if the removed node exists
				{
					delete removedNode;//delete it
				}
				return;//exit
			}
			else//otherwise
			{
				node<T>* removedNode = Remove(key, root);//find the node to remove
				if (removedNode != nullptr)//if the removed node exists
				{
					delete removedNode;//delete it
				}
				return;//exit
			}
		}
	}

private:
	/// <summary> Destroys the BST. </summary>
	/// <param name="branch"> Branch to delete from. </param>
	void DestroyTree(node<T>* branch)
	{
		if (branch != nullptr)//if the branch exists
		{
			//Recursively destroy the child branches, then delete the branch.
			DestroyTree(branch->left);
			DestroyTree(branch->right);
			delete branch;
		}
	}

	/// <summary> Insert a key into the BST. </summary>
	/// <param name="key"> Value to be inserted into the tree. </param>
	/// <param name="branch"> Branch to search from. </param>
	void Insert(T key, node<T>* branch)
	{
		if (key < branch->value)//if the value is less than the current branches value
		{
			if (branch->left != nullptr)//if the left branch exists
			{
				Insert(key, branch->left);//insert the value
			}
			else//otherwise
			{
				//create the branch and assign the value
				branch->left = new node<T>();
				branch->left->value = key;
				branch->left->left = nullptr;
				branch->left->right = nullptr;
				branch->left->parent = branch;
				branch->left->height = 1;
			}
		}
		else if (key >= branch->value)//if the value is greater than or equal to the current branches value
		{
			if (branch->right != nullptr)//if the right branch exists
			{
				Insert(key, branch->right);//insert the value
			}
			else//otherwise
			{
				//create the branch and assign the value.
				branch->right = new node<T>();
				branch->right->value = key;
				branch->right->left = nullptr;
				branch->right->right = nullptr;
				branch->right->parent = branch;
				branch->right->height = 1;
			}
		}

		//update the height of the parent branch
		branch->height = 1 + max(Height(branch->left), Height(branch->right));
	
		//get the balance of the branch to see whether it is unbalanced.
		int balance = GetBalance(branch);

		//Left left case
		if (balance > 1 && key < branch->left->value)
		{
			branch = RotateRight(branch);
			return;
		}

		//right right case
		if (balance < -1 && key > branch->right->value) 
		{
			branch = RotateLeft(branch);
			return;
		}

		//left right case
		if (balance > 1 && key > branch->left->value)
		{
			branch->left = RotateLeft(branch->left);
			branch = RotateRight(branch);
			return;
		}

		//right left case
		if (balance < -1 && key < branch->right->value)
		{
			branch->right = RotateRight(branch->right);
			branch = RotateLeft(branch);
			return;
		}

	}

	/// <summary> Finds a node with the value. </summary>
	/// <param name="key"> Value to search for. </param>
	/// <param name="branch"> Branch to search from. </param>
	/// <returns> Returns node with key value. </returns>
	node<T>* Search(T key, node<T>* branch)
	{
		if (branch != nullptr)//if the branch exists
		{
			if (key == branch->value)//if the value equals this branches value
			{
				return branch;//return this branch
			}
			if (key < branch->value)//if the value is less than this branches value
			{
				return Search(key, branch->left);//search the left branch
			}
			else//if the value is greater than this branches value
			{
				return Search(key, branch->right);//search the right branch.
			}
		}
		else//otherwise
		{
			return nullptr;//node not found, return null
		}
	}

	/// <summary> Finds the node with the maximum value on the given branch. </summary>
	/// <param name="branch"> Branch to search from. </param>
	/// <returns> Returns node with maximum key value. </returns>
	node<T>* Maximum(node<T>* branch)
	{
		if (branch->right != nullptr)//if the right branch isn't empty
		{
			//recursively find the right most branch
			return Maximum(branch->right);
		}
		else//otherwise
		{
			return branch;//this is the maximum value, return this branch
		}
	}

	/// <summary> Finds the node with the minimum value on the given branch. </summary>
	/// <param name="branch"> Branch to search from. </param>
	/// <returns> Returns node with minimum key value. </returns>
	node<T>* Minimum(node<T>* branch)
	{
		if (branch->left != nullptr)//if the left branch isn't empty
		{
			//recursively find the left most branch
			return Minimum(branch->left);
		}
		else//otherwise
		{
			return branch;//this is the minimum value, return this branch
		}
	}

	/// <summary> Removes a node with the value from the branch. </summary>
	/// <param name="key"> Value to search for. </param>
	/// <param name="branch"> Branch to remove from. </param>
	/// <returns> Returns the node with the value on the branch. </returns>
	node<T>* Remove(T key, node<T>* branch)
	{
		if (key < branch->value)//if the value is on the branches left side
		{
			if (branch->left != nullptr)//if the left side is not empty
				return Remove(key, branch->left);//recursively remove the node from the left side
			else//otherwise
				return nullptr;//the value doesn't exist in the tree, return null
		}
		else if (key > branch->value)//if the value is on the branches right side
		{
			if (branch->right != nullptr)//if the right side is not empty
				return Remove(key, branch->right);//recursively remove the node from the right side
			else//otherwise
				return nullptr;//the value doesn't exist in the tree, return null
		}
		else//otherwise, this is the node to remove
		{
			///Restructure the tree as appropriate

			if (branch->left != nullptr && branch->right != nullptr)//if the branch has two child branches
			{
				branch->value = Minimum(branch->right)->value;
				return Remove(branch->value, branch->right);
			}
			else if (branch->parent->left == branch)//if this branch is the left branch
			{
				branch->parent->left = (branch->left != nullptr) ? branch->left : branch->right;
				return branch;
			}
			else if (branch->parent->right == branch)//if this branch is the right branch
			{
				branch->parent->right = (branch->left != nullptr) ? branch->left : branch->right;
				return branch;
			}
		}
	}

	/// <summary> Returns the height value of a node. </summary>
	/// <param name="n"> Node to find height value of. </param>
	/// <returns> Returns the height value of the node. </returns>
	int Height(node<T>* n)
	{
		if (n == nullptr)
		{
			return 0;
		}
		return n->height;
	}

	/// <summary> Returns the maximum value between to values. </summary>
	/// <param name="a"> First value to compare. </param>
	/// <param name="b"> Second value to compare. </param>
	/// <returns> Returns the maximum value between to values. </returns>
	int max(int a, int b)
	{
		return (a > b) ? a : b;
	}

	/// <summary> Rotates a branch to the right. </summary>
	/// <param name="branch"> Branch to rotate. </param>
	/// <returns> Returns the rotated branch. </returns>
	node<T>* RotateRight(node<T>* branch)
	{
		node<T>* x = branch->left;
		if (x != nullptr)
		{
			node<T>* T2 = x->right;
			if (T2 != nullptr)
			{
				if (branch->parent != nullptr)
				{
					if (branch->parent->left == branch)
						branch->parent->left = x;
					else if (branch->parent->right == branch)
						branch->parent->right = x;
				}

				x->parent = branch->parent;
				branch->parent = x;
				T2->parent = branch;

				x->right = branch;
				branch->left = T2;

				branch->height = max(Height(branch->left), Height(branch->right)) + 1;
				x->height = max(Height(x->left), Height(x->right)) + 1;

				return x;
			}
			return branch;
		}
		return branch;		
	}

	/// <summary> Rotates a branch to the left. </summary>
	/// <param name="branch"> Branch to rotate. </param>
	/// <returns> Returns the rotated branch. </returns>
	node<T>* RotateLeft(node<T>* branch)
	{
		node<T>* y = branch->right;
		if (y != nullptr)
		{
			node<T>* T2 = y->left;
			if (T2 != nullptr)
			{
				if (branch->parent != nullptr)
				{
					if (branch->parent->left == branch)
						branch->parent->left = y;
					else if (branch->parent->right == branch)
						branch->parent->right = y;
				}
				y->parent = branch->parent;
				branch->parent = y;
				T2->parent = branch;

				y->left = branch;
				branch->right = T2;

				branch->height = max(Height(branch->left), Height(branch->right)) + 1;
				y->height = max(Height(y->left), Height(y->right)) + 1;

				return y;
			}
			return branch;
		}		
		return branch;
	}

	/// <summary> Returns the balance of a branch. </summary>
	/// <param name="n"> Branch to get the balance of. </param>
	/// <returns> Returns the balance value of a branch. </returns>
	int GetBalance(node<T>* n)
	{
		if (n == nullptr)
			return 0;

		return Height(n->left) - Height(n->right);
	}

	node<T>* root; //The root node.
};
#endif // !_BST_H_