#ifndef _LINKEDLIST_H_
#define _LINKEDLIST_H_
#include "LinkedListNode.h"
#include <string>
template<typename T>
class LinkedList
{
public:

	LinkedList() 
	{
		head = nullptr;//set the head to null by default
		currentNode = head;//set the current node to the head by default
	}

	~LinkedList() {}

	/// <summary> Returns whether or not the list contains the value. </summary>
	/// <param name="value"> </param>
	/// <returns> bool </returns>
	bool Contains(T value) const
	{
		bool ret = false;
		const LinkedListNode<T>* currentNode = head;//create a node pointer to iterate
		while (currentNode != nullptr)
		{
			if (currentNode->GetValue() == value)//if the node's value equals the value
			{
				ret = true;//return true
				break;
			}
			currentNode = currentNode->GetNext();//set the current node to its next node
		}
		return false;//return false
	}

	/// <summary> Finds and returns the index of the first node in the list with T value. </summary>
	/// <param name="value"> </param>
	/// <returns> int </returns>
	int IndexOf(T value)
	{
		LinkedListNode<T>* currentNode = head;//create a node pointer to iterate
		int index = 0;
		while (currentNode != nullptr)
		{
			if (currentNode->GetValue() == value)//if the node's value equals the value
			{
				return index;//return the index
			}
			currentNode = currentNode->GetNext();//set the current node to its next node
			index++;
		}
		return -1;
	}


	/// <summary> Inserts T value into the list. </summary>
	/// <param name="value"> </param>
	void Insert(T value)
	{
		LinkedListNode<T>* newNode = new LinkedListNode<T>();//creates a new node of type T
		newNode->SetValue(value);//set the value of the node
		if (head == nullptr)//if the head is null, meaning this is the first value
		{
			head = newNode;//set the head equal to the new node
		}
		else//otherwise
		{
			currentNode->SetNext(newNode);//set the current nodes next node to the new node
			newNode->SetPrevious(currentNode);//set the new nodes previous node to the current node
		}
		size++;
		currentNode = newNode;//set the current node to the new node

	}

	/// <summary> Finds and delets the first node in the list with T value. </summary>
	/// <param name="value"> </param>
	void Delete(T value)
	{
		LinkedListNode<T>* current = head;//create a node pointer to iterate

		if (value == head->GetValue())
		{
			if (head->GetNext() != nullptr)//if there is a next node
			{
				head = head->GetNext();//set the node to the current nodes next node
			}
		}
		else
		{
			current = current->GetNext();

			while (current != nullptr)
			{
				if (current->GetValue() == value)//if the current iterator node's value is equal to the value
				{
					if (current->GetPrevious() != nullptr)
					{
						current->GetPrevious()->SetNext(current->GetNext());//set the current node's previous node's next node to the current node's next node
					}

					if (current->GetNext() != nullptr)
					{
						current->GetNext()->SetPrevious(current->GetPrevious());//set the current node's next node's previous node to the current node's previous node
					}

					break;//exit the function
				}
				current = current->GetNext();//current equals current node's next node
			}
		}

		//delete the current node
		delete current;
		current = nullptr;
		size--;
		
	}

	/// <summary> Finds and returns the minimum value in the list. </summary>
	/// <returns> T </returns>
	T Minimum()
	{
		T min = T();
		if (head != nullptr)
		{
			min = head->GetValue();//set the minimum value to the head node's value
		}
		LinkedListNode<T>* current = head;
		while (current != nullptr)
		{
			if (current->GetValue() < min)//if the current nodes value is less than the min value
			{
				min = current->GetValue();//set the min value to the current node's value
			}
			current = current->GetNext();//current node equals next node
		}
		return min;//return minimum value
	}

	/// <summary> Finds and returns the maximum value in the list. </summary>
	/// <returns> T </returns>
	T Maximum()
	{
		T max = T();
		if (head != nullptr)
		{
			max = head->GetValue();//set the max value to the head node's value
		}
		LinkedListNode<T>* current = head;
		while (current != nullptr)
		{
			if (current->GetValue() > max)//if the current nodes value is greater than the max value
			{
				max = current->GetValue();//set the max value to the current node's value
			}
			current = current->GetNext();//current node equals next node
		}
		return max;//return maximum value
	}

	/// <summary> Returns the number of elements in the list. </summary>
	/// <returns> int </returns>
	int Size()
	{
		return size;
	}

	/// <summary> Returns the last node in the list. </summary>
	/// <returns> Node* </returns>
	const LinkedListNode<T>* GetLast() const
	{
		return currentNode;
	}

	/// <summary> Returns a reference to the value at the index. </summary>
	/// <param name="index"> </param>
	/// <returns> T& </returns>
	T& operator [] (int index) const
	{
		LinkedListNode<T>* ret = head;
		for (size_t i = 0; i < index; i++)
		{
			ret = ret->GetNext();
		}
		return ret->GetValue();
	}


private:
	LinkedListNode<T>* head;//the head node of the list
	LinkedListNode<T>* currentNode;//current last node
	int size = 0;


};

#endif