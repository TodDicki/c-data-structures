#ifndef _LINKEDLISTNODE_H_
#define _LINKEDLISTNODE_H_

template<typename T>
class LinkedListNode
{
public:

	
	LinkedListNode()
	{
		//set next and previous to null by default
		next = nullptr;
		previous = nullptr;
	}
	~LinkedListNode() {}

	/// <summary> Returns the previous Node. </summary>
	/// <returns> Node* </returns>
	LinkedListNode<T> * GetPrevious()
	{
		return previous;
	}

	/// <summary> Returns the next Node. </summary>
	/// <returns> Node* </returns>
	LinkedListNode<T> * GetNext()
	{
		return next;
	}

	/// <summary> Returns the value of the node. </summary>
	/// <returns> T& </returns>
	T& GetValue()
	{
		return value;
	}

	/// <summary> Set the previous Node. </summary>
	/// <param name="prev"> Node * </param>
	void SetPrevious(LinkedListNode<T> * prev)
	{
		previous = prev;
	}

	/// <summary> Set the next Node. </summary>
	/// <param name="n"> Node * </param>
	void SetNext(LinkedListNode<T> * n)
	{
		next = n;
	}

	/// <summary> Set the value of the node. </summary>
	/// <param name="val"> T </param>
	void SetValue(T val)
	{
		value = val;
	}


private:
	LinkedListNode<T>* previous;//previous node in list
	LinkedListNode<T>* next;//next node in list
	T value;//value of node
};

#endif