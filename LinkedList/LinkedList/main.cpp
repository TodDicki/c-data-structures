#include "LinkedList.h"
#include <iostream>

int main()
{
	LinkedList<int> list;//create a linked list of ints

	for (int i = 0; i < 10; i++)
	{
		list.Insert(i);
	}
	
	for (int i = 0; i < 10; i++)
	{
		std::cout << list[i] << std::endl;
	}
	
	list[3] = 5;
	list[2] = 99;
	list[7] = 8;
	
	std::cout << std::endl;
	
	for (int i = 0; i < 10; i++)
	{
		std::cout << list[i] << std::endl;
	}

	std::cin.get();//wait for input

	return 0;
}