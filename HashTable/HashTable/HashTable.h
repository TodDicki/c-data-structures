#ifndef _HASHTABLE_H_
#define _HASHTABLE_H_

template<typename K, typename V>
class HashNode
{
public:

	///<summary>Default constructor</summary> 
	///<param name = 'key'> Key part of key value pair< / param>
	///<param name = 'value'> Value part of key value pair< / param>
	///<returns>HashNode</returns> 
	HashNode(const K &key, const V &value) : key(key), value(value), next(nullptr)
	{}

	///<summary>Returns the key of the HashNode</summary> 
	///<returns> K </returns> 
	K GetKey() const
	{
		return key;
	}

	///<summary> Returns the Value of the HashNode</summary> 
	///<returns> V </returns> 
	V GetValue() const
	{
		return value;
	}

	///<summary> Sets the Value of the HashNode </summary> 
	///<param name = 'v'> New value < / param>
	void SetValue(V v)
	{
		value = v;
	}

	///<summary> Returns a pointer to the next HashNode </summary> 
	///<returns> HashNode * </returns> 
	HashNode* GetNext() const
	{
		return next;
	}

	///<summary> Sets the next HashNode </summary> 
	///<param name = 'n'> Pointer to the next HashNode < / param>
	void SetNext(HashNode* n)
	{
		next = n;
	}

private:
	K key;
	V value;
	HashNode* next;

};

template<typename K, size_t tableSize>
struct KeyHash
{
	///<summary>Operator used to hash the key </summary> 
	///<param name = 'key'> Key to hash< / param>
	///<returns> int </returns> 
	int operator()(K &key) const
	{
		int hash = (std::hash<K>()(key) % tableSize);
		hash = abs(hash);
		return hash;
	}

};


template<typename K, typename V, size_t tableSize, typename F = KeyHash<K, tableSize>>
class HashTable
{
public:

	///<summary>Constructor</summary> 
	HashTable()
	{
		//initialize table of with the appropriate size
		table = new HashNode<K, V> *[tableSize]();
	}

	///<summary>Destructor</summary> 
	~HashTable()
	{
		//delete all elements in the table
		for (size_t i = 0; i < tableSize; i++)
		{
			HashNode<K, V> *entry = table[i];
			while (entry != null)
			{
				HashNode<K, V> *prev = entry;
				entry = entry->GetNext();
				delete prev;
			}
			table[i] = nullptr;
		}
		//delete the table
		delete [] table;
	}

	///<summary>Returns the node with the Key Value</summary> 
	///<param name = 'key'> Key to search for< / param>
	///<returns>HashNode</returns> 
	HashNode<K, V>* Get(K &key)
	{
		unsigned long hashValue = hashFunc(key);//hash the key

		HashNode<K, V>* ret = table[hashValue];//grab the first node in the table with the hash value

		//iterate over the linked list of nodes until the correct one is found
		while (ret != nullptr)
		{
			if (ret->GetKey() == key)
			{
				return ret;
			}
			ret = ret->GetNext();
		}

		//if the key is not in the table, then return null
		return nullptr;
	}


	///<summary>Adds a node with the key value pair.</summary> 
	///<param name = 'key'> Key part of key value pair< / param>
	///<param name = 'value'> Value part of key value pair< / param>
	void Add(K &key, const V &value)
	{
		unsigned long hashValue = hashFunc(key);//hash the key
		HashNode<K, V>* prev = nullptr;
		HashNode<K, V>* entry = table[hashValue];//grab the first entry in the linked list that corresponds to the hash

		//while there is an element in the current position, grab the next position in the linked list
		while (entry != nullptr && entry->GetKey() != key)
		{
			prev = entry;
			entry = entry->GetNext();
		}

		//insert the value at the position
		if (entry == nullptr)
		{
			entry = new HashNode<K, V>(key, value);
			if (prev == nullptr)
			{
				table[hashValue] = entry;
			}
			else
			{
				prev->SetNext(entry);
			}
		}
		else
		{
			entry->SetValue(value);
		}
	}

	///<summary>Removes the node associated with the key. </summary> 
	///<param name = 'key'> Key to search for.< / param>
	void Remove(K &key)
	{
		unsigned long hashValue = hashFunc(key);//hash the key
		HashNode<K, V>* prev = nullptr;
		HashNode<K, V>* entry = table[hashValue];//grab the first entry in the linked list that corresponds to the hash

		//while there is an element in the current position, grab the next position in the linked list
		while (entry != nullptr && entry->GetKey() != key)
		{
			prev = entry;
			entry = entry->GetNext();
		}

		//Remove the key value pair, if it exists
		if (entry == nullptr)
		{
			return;
		}
		else
		{
			if (prev == nullptr)
			{
				table[hashValue] = entry->GetNext();
			}
			else
			{
				prev->SetNext(entry->GetNext());
			}
			delete entry;
		}
	}


private:
	HashNode<K, V> **table;
	F hashFunc;
};

#endif // !_HASHTABLE_H_