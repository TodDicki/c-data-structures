#include <random>
#include <iostream>
#include <string>
#include <stdlib.h>
#include "HashTable.h"

const char alphanum[] =
"0123456789"
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz";

//randomly generates strings of length
const char* GenerateString(int length)
{
	char* ret = new char[length];
	for (int i = 0; i < length; i++)
	{
		ret[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
	}
	return ret;
}

int main()
{
	HashTable<std::string, int, 1000000>* table = new HashTable<std::string, int, 1000000>();

	//size of strings to insert
	int minStringLength = 1;
	int maxStringLength = 100;

	int minNum = 100;
	int maxNum = 5000;

	std::string toFind;

	//create and insert 100000 randomly generated strings of varying length
	for (size_t i = 0; i < 100000; i++)
	{
		toFind = std::string(GenerateString((rand() % maxStringLength + minStringLength)));
		table->Add(toFind, (rand() % maxNum) + minNum);
	}
	//find the node
	HashNode<std::string, int>* found = table->Get(toFind);

	//Print info
	if (found != nullptr)
	{
		std::cout << "Value for " << toFind << " is " << found->GetValue() << std::endl;
	}
	else
	{
		std::cout << "Value for " << toFind << " not found." << std::endl;
	}

	//wait for input
	std::cin.get();



	return 0;
}